let modal = document.querySelector('.modal-window')
let loginbtn = document.querySelector('.loginbtn').addEventListener("click", function(){
    modal.classList.remove("close")
    modal.classList.add("open")
});

let closebtn = document.querySelector('.fa-times').addEventListener("click", function(){
    modal.classList.remove("open")
    modal.classList.add("close")
})

let shoppingBtn = document.querySelector('.fa-shopping-bag').addEventListener("click", function(){
    let shoppingBag = document.querySelector('.shopping-bag')
    shoppingBag.classList.toggle('showBag')
})

let bag = {} // корзина


// const carsMenu = document.querySelector('.cars-menu');
// function menu() {
//     let json = function loadGoods() {
//         // загружаю товары на страницу
//         fetch("goods.json")
//             .then(Response => Response.json())
//             .then(json => console.log(json))
//     }
//     for(let i = 0; i < 5; i++) {
//         carsMenu.innerHTML += '<div class="card"><h2 class="name">'+json[i]['name']+'</h2></div>'
//     }
// }
// menu()

$('document').ready(function(){
    $.getJSON('goods.json', function(data) {
        let goods = data
        loadGoods()
        checkBag()
        showBag()
        function showBag() {
            let out = ''
            for (let key in bag){
                out += '<div class="shopping-card">'
                out += '<div class="image" style="background-image: url('+goods[key]['img']+')"></div>'
                out += '<h2 class="title">'+goods[key]["name"]+'</h2>'
                out += '<p class="price">$'+goods[key]["cost"]+'</p>'
                out += '</div>'
                out += '<button class="clear-btn">x</button>'
            }
            $('.shopping-bag').html(out)
            $('.clear-btn').on("click", clearToBag)
        }
    })
   
})

function clearToBag() {
    $('.shopping-card').css("display", "none")
    localStorage.clear()
}

function loadGoods() {
    //загружаю товары на страницу
    $.getJSON('goods.json', function(data){
        let out = ''
        for (let key in data){
            out += '<div class="card" style="padding: 5px">'
            out += '<div class="image" style="background-image: url('+data[key]['img']+')"></div>'
            out += '<h2 class="title">'+data[key]['name']+'</h2>'
            out += '<p class="price">$'+data[key]['cost']+'</p>'
            out += '<p class="price">'+data[key]['description']+'</p>'
            out += '<a href="#" class="btn-buy" data-art="'+key+'">Buy</a>'
            out += '</div>'
        }
        $('.cars-menu').html(out)
        $('.btn-buy').on("click", addToBag)
    })
}

function addToBag() {
    // добавляю товар в корзину
    let article = $(this).attr('data-art')
    if (bag[article] != undefined) {
        bag[article]++
    }
    else {
        bag[article] = 1
    }
    localStorage.setItem('bag', JSON.stringify(bag))
    // console.log(bag)
    showBag()
}


function checkBag() {
    // проверка на товар в корзине (localStorage)
    if(localStorage.getItem('bag') != null) {
        bag = JSON.parse(localStorage.getItem('bag'))
    }
}
